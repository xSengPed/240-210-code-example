class Monster {
    private boolean isAlive = true;
    private int hp;
    private String name;
    private int atk;
    Monster(String name,int hp,int atk)
    {
        this.name = name;
        this.hp = hp;
        this.atk = atk;

    }
    public void attack(Character Target) {
        Target.onAttack(this.atk);
    }
    public void onAttack(int attack){
        this.hp -= attack;
        if(this.hp <= 0 ){
            isAlive = false;
        }
    }
    @Override
    public String toString(){
        String status = " Name : " + this.name + " HP : " + this.hp + " Alive : " + this.isAlive;
        return status;
    }
}