class Main{
    public static void main(String args[]) {
        Magician Donnukrit = new Magician("DONNUKRIT", 250, 300);
        Monster Zombie = new Monster("Zombie", 400, 15);
        Donnukrit.firebolt(Zombie);
        Donnukrit.firebolt(Zombie);
        Donnukrit.lightning_bolt(Zombie);
        Donnukrit.lightning_bolt(Zombie);
        Donnukrit.lightning_bolt(Zombie);
        Donnukrit.lightning_bolt(Zombie);
        Donnukrit.lightning_bolt(Zombie);
        Donnukrit.lightning_bolt(Zombie);
        Donnukrit.lightning_bolt(Zombie);
        Donnukrit.printStatus();
        System.out.print(Zombie.toString());
    }
}