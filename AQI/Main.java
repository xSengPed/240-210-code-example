import java.util.HashMap;
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        HashMap<String, Integer> airQuality = new HashMap<String, Integer>();

        airQuality.put("Excellent", 0);
        airQuality.put("Good", 0);
        airQuality.put("Ligthly", 0);
        airQuality.put("Moderately", 0);
        airQuality.put("Heavily", 0);

        while (true) {
            System.out.print("Enter Aqi [Q or q for Exit ] : ");
            String data = sc.nextLine();
            if (data.equals("Q") || data.equals("q")) {
                break;
            } else {
                int aqi = Integer.parseInt(data);
                int idx;

                if (aqi >= 0 && aqi <= 25) { // Excellent
                    idx = airQuality.get("Excellent");
                    airQuality.put("Excellent", idx + 1);
                } else if (aqi >= 26 && aqi <= 50) {
                    idx = airQuality.get("Good");
                    airQuality.put("Good", idx + 1);
                } else if (aqi >= 51 && aqi <= 100) {
                    idx = airQuality.get("Ligthly");
                    airQuality.put("Ligthly", idx + 1);
                } else if (aqi >= 101 && aqi <= 200) {
                    idx = airQuality.get("Moderately");
                    airQuality.put("Moderately", idx + 1);
                }
                else {
                    idx = airQuality.get("Heavily");
                    airQuality.put("Heavily", idx + 1);
                }
            }
        }

        System.out.println(airQuality);
    }
}
