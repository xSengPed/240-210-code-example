class main {
    public static void main(String[] args) {
        
        Shape shapes[] = new Shape[4];
        shapes[0] = new Sphere("S1", 10);
        shapes[1] = new Sphere("S2", 20);
        shapes[2] = new Cone("C1", 5, 10);
        shapes[3] = new Cone("C2", 7.5, 10.5);

        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].getname()+" "+shapes[i].getVolume());
        }

        Shape longest = findLongest(shapes);
        System.out.println("The Longest is "+longest.getname());
    }

    public static Shape findLongest(Shape[] shapes) {
        Shape longest = new Shape("");
        for (int i = 0; i < shapes.length; i++) {
            if (longest.getVolume() < shapes[i].getVolume()) {
                longest = shapes[i];
            }
        }
        return longest;
    }
}