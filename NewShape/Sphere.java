class Sphere extends Shape {
    private double radius;
    Sphere(String _name,double r){
        super(_name);
        this.radius = r;
    }

    public double getVolume(){
        double v = 4.0/3 * Math.PI * radius * radius * radius;
        return v;
    }
}