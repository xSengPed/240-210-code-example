class Pokemon {
    private String name;
    private int maxHp;
    private int Hp;

    Pokemon(String _name){
        this.name = _name;
        this.maxHp = (int)(Math.random() * 100) + 1;
        this.Hp = (int)(Math.random()*this.maxHp) + 1;
    }

    public String getName() {
        return this.name;
    }

    public void eat(){
        if(this.Hp < this.maxHp-10) {
            this.Hp += 10;
        }
        else {
            this.Hp = this.maxHp;
        }
    }
    // @Override // Annotation
    public String toString(){
        return this.name + " " + this.Hp + "/" + this.maxHp;
    }
}