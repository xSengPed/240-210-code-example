import java.util.ArrayList;

class PokemonCenter {
    private ArrayList<Pokemon> pokemonList;
    
    PokemonCenter(){
        pokemonList = new ArrayList<Pokemon>();
    }
    public void addPoke(Pokemon p){
        pokemonList.add(p);
    }
    public void feed(){
       // for(int i = 0; i < pokemonList.size(); i++){ // ORIGINAL:
       //     pokemonList.get(i).eat();
       //  }

       for(Pokemon p : pokemonList){ // ท่า For Each
            p.eat();
       }
    }
    public void printPokemon(){
        // for(int i = 0; i < pokemonList.size(); i++){ // ORIGINAL:
        //   System.out.println(pokemonList.get(i).toString());
        // }

        for(Pokemon p : pokemonList){ // ท่า For Each
            System.out.println(p.toString());
        }
    }
}