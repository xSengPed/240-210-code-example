===Midterm===

Content
    
    คลาสและออบเจกต์ (Class & Object)
        Class
            - Object
        Method
            - Constructor
            - Mutator & Accessor
                - Setter
                - Getter

    ตัวแปรและชนิดตัวแปรพื้นฐาน (Variable & Variable Type)
    - Primative Type
        - int
        - double
        - float
    - Reference Type
        - Class
        - String
        - Array
        
    Collection & Library
        - Library
        - ArrayList
        - Iterator
        - Hashmap
        - Collection
        - String Method

    การสืบทอดคลาส (Inheritance)
        - Super Class
        - Sub Class
        - Super Constructor

===Final===

    Abstract & Interface (คลาสนิยามและอินเตอเฟส)
        - Concrete Class
        - Abstract Class
        - Interface

    คุณสมบัติหลายรูปแบบ / ภาวะพ้องรูป (Polymorphism)
        - Method Overriding
    
    Graphic User Interface (GUI) Programing

    Exception (การจัดการข้อผิดพลาด)
    
    Thread and Concurrent (เธรด และ การทำงานแบบร่วม)