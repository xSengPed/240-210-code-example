class Fan {
    private String Brand;
    private boolean isSwing;
    private int Speed;


    Fan(){}
    Fan(String _Brand,int _Speed){
        this.Brand = _Brand;
        this.isSwing = false;
        
        if(_Speed > 3 || _Speed < 0 ){
            System.out.println("Speed must between 0 - 3");
        }
        else {
            this.Speed = _Speed;
        }
    }

    // Mutator
    public void setBrand(String _Brand){
        this.Brand = _Brand;
    }
    public void setSwing(boolean _isSwing)
    {
        this.isSwing = _isSwing;
    }
    public void setSpeed(int _Speed){
        if(_Speed > 3 || _Speed < 0 ){
            System.out.println("Speed must between 0 - 3");
        }
        else {
            this.Speed = _Speed;
        }
    }
    // Accessor
    public String getBrand() {
        return this.Brand;
    }
    public boolean getIsSwing(){
        return this.isSwing;
    }
    public int getSpeed() {
        return this.Speed;
    }
    // Method Print Status
    public void printStatus(){
        System.out.println("Brand : "+this.Brand);
        System.out.println("Swinged : "+this.isSwing);
        System.out.println("Speed : "+this.Speed+"\n");
    }

}